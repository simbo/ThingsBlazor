﻿<div align="center"><h1 align="center">ThingsBlazor</a></h1></div>
<div align="center"><h3 align="center">权限管理框架</h3></div>

### 🎁 框架介绍
 一个通用的AspNetCore小型权限框架，采用NET6/7(Furion/SqlSugar)+BlazorServer，实现RBAC权限管理，在线会话管理，并建立Api第三方授权模式。

### 📙 框架技术栈
 - 👉 NET框架：[Furion](https://furion.baiqian.ltd/docs)
 - 👉 Orm：[SqlSugar](https://www.donet5.com/home/doc)
 - 👉 Blazor组件：[Masa.Blazor](https://www.masastack.com/blazor)
 - ......


### 🍎 效果图
 <table>
    <tr>
        <td><img src="https://gitee.com/diego2098/ThingsBlazor/raw/master/Image/1.png"/></td>
        <td><img src="https://gitee.com/diego2098/ThingsBlazor/raw/master/Image/2.png"/></td>
        <td><img src="https://gitee.com/diego2098/ThingsBlazor/raw/master/Image/3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/diego2098/ThingsBlazor/raw/master/Image/4.png"/></td>
        <td><img src="https://gitee.com/diego2098/ThingsBlazor/raw/master/Image/5.png"/></td>
        <td><img src="https://gitee.com/diego2098/ThingsBlazor/raw/master/Image/6.png"/></td>
    </tr>
 </table>

### 📙 脚手架

 ```powershell
 dotnet new install Things.Blazor
 ```

 ### 📙 衍生作品
  - 👉[ThingsGateway](https://gitee.com/diego2098/ThingsGateway)
    跨平台边缘采集网关

### 💐 特别鸣谢/源码参考(引用)
 - 👉[Simple.Admin](https://gitee.com/zxzyjs/SimpleAdmin)
   前端小诺2.0(Vue3),后端采用Furion+Sqlsugar
 - 👉[Masa.Blazor.Docs](https://github.com/BlazorComponent/MASA.Blazor/tree/main/docs)
   Masa.Blazor官网站点
 - ......

### 🎀 支持作者
 如果对您有帮助，请点击右上角⭐Star关注或扫码捐赠，感谢支持开源！
  <table>
    <tr>
        <td><img height=200 src="https://gitee.com/diego2098/ThingsBlazor/raw/master/Image/pay.png"/></td>
    </tr>
 </table>

### 🍎 联系作者
 * QQ群：605534569
 * 邮箱：2248356998@qq.com