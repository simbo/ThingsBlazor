﻿namespace ThingsBlazor.Web.Entry
{
    public class SingleFilePublish : ISingleFilePublish
    {
        public Assembly[] IncludeAssemblies()
        {
            return Array.Empty<Assembly>();
        }

        public string[] IncludeAssemblyNames()
        {
            return new[]
            {
            "ThingsBlazor.Foundation",
            "ThingsBlazor.Web.Foundation",
            "ThingsBlazor.Web.Page",

            "ThingsBlazor.Web.Rcl",
            "ThingsBlazor.Web.Rcl.Core",
            "ThingsBlazor.Core",
            "ThingsBlazor.Web.Core"
        };
        }
    }
}