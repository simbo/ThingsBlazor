﻿namespace ThingsBlazor.Web.Rcl.Core
{
    public interface IAppDataTable
    {
        public Task QueryClick();
    }
}